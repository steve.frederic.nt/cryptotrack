import Head from 'next/head';
import CoinList from '../components/CoinList';
import Layout from '../components/Layout';


export default function Home({filteredCoins}) {

  const allCoins = filteredCoins.filter(coin=>
    coin.name.toLowerCase()
    )

  return (
    <div>
    <Head>
    <link rel="shortcut icon" type="image/ico" href="/favicon.ico" />
    </Head>
    <Layout>
        <div className="coin_app">
          <h1>Top 20 Cryptocurrencies App</h1>
          <br/>
          <h4>Written in</h4>
          <img src="/nextjs.svg" alt="NextJS" height="124px" width="207px" />
          <CoinList filteredCoins={allCoins} />
        </div>
    </Layout>
    </div>
  )
}


export const getServerSideProps = async () => {
  const res = await fetch('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=20&page=1&sparkline=false');

  const filteredCoins = await res.json();

  return{
    props: {
      filteredCoins
    }
  };
};
