module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['assets.coingecko.com'],
  },
  experimental: {
    outputStandalone: true,
  },
}
