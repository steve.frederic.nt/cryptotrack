# Welcome to Cryptotrack
![Version](https://img.shields.io/badge/version-0.1.1-blue.svg?cacheSeconds=2592000)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/steve.frederic.nt/cryptotrack)
[![License: GPLV3](https://img.shields.io/badge/License-GPLV3-yellow.svg)](https://www.gnu.org/licenses/gpl-3.0.en.html)
<br />
<br />

## 🏠 [Homepage](https://gitlab.com/steve.frederic.nt/cryptotrack)  
<br />

> **Cryptotrack** is a PoC (Proof of Concept) written in NextJS which allows you to follow the **TOP 20 Cryptos**!

<br />
<br />

## Requirements
* [Bash](https://www.gnu.org/software/bash/)
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* A [Gitlab.com](https://gitlab.com/) account for code repository, image repository, and CI/CD.
* [Docker](https://docs.docker.com/engine/install/)
* [Terraform](https://www.terraform.io/downloads)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* [Kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)

## Tutorial
* Download the source code:
```sh
git clone git@gitlab.com:steve.frederic.nt/cryptotrack.git
```
* To manually build the docker image (from the root directory):
```sh
docker build --rm -t registry.gitlab.com/<your gitlab username>/cryptotrack/cryptotrack:<your tag> -t ./dockerize
```
  You will be able to use your new build in the Kubernetes manifest:<br />
  k8s-aws/modules/ansible/roles/install_cryptotrack/files/cryptotrack.yaml at line **24** after you push it to your registry:
```sh
docker push registry.gitlab.com/<your gitlab username>*/cryptotrack/cryptotrack:<your tag>
```
* To automacitally build a new image if the Dockerfile is modified:
  - Use the Gitlab CI/CD by copying and modifying the .gitlab-ci.yml in the root directory.
  - Add your variables under Settings > CI/CD > Variables.
  - Modify the Dockerfile file to get a new build.
<br />
<br />

* To deploy the app to AWS programatically:
  - Look at the k8s-aws/variables.tf file
  - Create a terraform.tfvars with your own variables (including AWS credentials)
  - Then, from the k8s-aws directory, run:
```sh
terraform init
```
Then, to make sure there are no issues with the variables:
```sh
terraform validate
```
and
```sh
terraform plan
```
When everything checks out, run:
```sh
terraform apply -auto-approve
```
When the script finishes, it will give you an IP address. Use it to check out the app. It might take a couple of minutes for the address to actually load anything as the deployment is still taking place in the background.

Enjoy!

## TODO

- [x] Automate docker image build
- [ ] Use domain name and TLS for LetsEncrypt https
- [ ] Add Terraform and Ansible to CI/CD process in a secure manner
- [ ] Replace the CoinGecko API with a selfmade Python API: The API will fetch live and historical data dumping it to S3

## Author

👤 **Steve N-T**

* Website: TODO
* Gitlab: [@steve.frederic.nt](https://gitlab.com/steve.frederic.nt)
* LinkedIn: [@https:\/\/www.linkedin.com\/in\/steve-nt](https://linkedin.com/in/https:\/\/www.linkedin.com\/in\/steve-nt)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/steve.frederic.nt/cryptotrack/-/issues).