# Connect to ec2 instance
resource "null_resource" "ansible_to_ec2" {
  connection {
    host        = var.node01_public_ip
    user        = "ubuntu"
    private_key = file("~/.ssh/aws_id_rsa")
  }

  provisioner "local-exec" {
    command = "printf '[controller]\n${var.node01_public_ip}' > ./modules/ansible/inventory/hosts.ini"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update -y",
      "sudo apt install python3 -y",
    ]
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i ./modules/ansible/inventory/hosts.ini -l ${var.node01_public_ip} --private-key ~/.ssh/aws_id_rsa ./modules/ansible/init.yml"
  }

  depends_on = [var.node01_public_ip]
}