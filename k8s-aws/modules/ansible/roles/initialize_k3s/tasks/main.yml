---

- name: Download k3s binary x64
  get_url:
    url: https://github.com/k3s-io/k3s/releases/download/{{ k3s_version }}/k3s
    checksum: sha256:https://github.com/k3s-io/k3s/releases/download/{{ k3s_version }}/sha256sum-amd64.txt
    dest: /usr/local/bin/k3s
    owner: root
    group: root
    mode: 0755
  when: ansible_facts.architecture == "x86_64"
  become: true

- name: Copy K3s service file
  register: k3s_service
  template:
    src: "k3s.service.j2"
    dest: "{{ systemd_dir }}/k3s.service"
    owner: root
    group: root
    mode: 0644
  become: true

- name: Enable and check K3s service
  systemd:
    name: k3s
    daemon_reload: yes
    state: restarted
    enabled: yes
  become: true

- name: Wait for node-token
  wait_for:
    path: "{{ k3s_server_location }}/server/node-token"
  become: true

- name: Register node-token file access mode
  stat:
    path: "{{ k3s_server_location }}/server/node-token"
  register: p
  become: true

- name: Change file access node-token
  file:
    path: "{{ k3s_server_location }}/server/node-token"
    mode: "g+rx,o+rx"
  become: true

- name: Read node-token from controller
  slurp:
    path: "{{ k3s_server_location }}/server/node-token"
  register: node_token
  become: true

- name: Store Controller node-token
  set_fact:
    token: "{{ node_token.content | b64decode | regex_replace('\n', '') }}"
  become: true

- name: Restore node-token file access
  file:
    path: "{{ k3s_server_location }}/server/node-token"
    mode: "{{ p.stat.mode }}"
  become: true

- name: Create directory .kube
  file:
    path: ~{{ ansible_user }}/.kube
    state: directory
    owner: "{{ ansible_user }}"
    mode: "u=rwx,g=rx,o="
  become: true

- name: Copy config file to user home directory
  copy:
    src: /etc/rancher/k3s/k3s.yaml
    dest: ~{{ ansible_user }}/.kube/config
    remote_src: yes
    owner: "{{ ansible_user }}"
    mode: "u=rw,g=,o="
  become: true

- name: Replace https://localhost:6443 by https://controller-ip:6443
  command: >-
    k3s kubectl config set-cluster default
      --server=https://{{ controller_ip }}:6443
      --kubeconfig ~{{ ansible_user }}/.kube/config
  changed_when: true

- name: Create kubectl symlink
  file:
    src: /usr/local/bin/k3s
    dest: /usr/local/bin/kubectl
    state: link
  become: true

- name: Create crictl symlink
  file:
    src: /usr/local/bin/k3s
    dest: /usr/local/bin/crictl
    state: link
  become: true